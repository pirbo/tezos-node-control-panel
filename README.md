A web-based graphical user interface for `tezos-node`

Get it either by:
- `opam pin add tezos-node-control-panel ...` or
- cloning the repository, installing the dependencies listed in the opam file and `dune build`

Run it by launching (`_build/install/default/bin/`)`tezos-node-control-panel`. Use `--endpoint` to specify the RPC server of the node to control (by default `http://localhost:8732`) and `-P` to specify on which port the control panel should be served (by default `18732`).

Enjoy it by browsering http://localhost:18732 (or the address you specified)
