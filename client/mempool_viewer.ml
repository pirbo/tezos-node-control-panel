(*****************************************************************************)
(*                                                                           *)
(* Tezos-node control panel                                                  *)
(*                                                                           *)
(* Copyright (c) 2021 Pierre Boutillier <pierre.boutillier@laposte.net>      *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* Where we duplicate types&encodings because we cannot compile octez to js *)

type generic_manager = {
  source : string;
  fee : string;
  counter : string;
  gas_limit : string;
  storage_limit : string;
}

let generic_manager_encoding =
  Data_encoding.(conv
                   (fun { source; fee; counter; gas_limit; storage_limit } ->
                      (source, fee, counter, gas_limit, storage_limit))
                   (fun  (source, fee, counter, gas_limit, storage_limit) ->
                       { source; fee; counter; gas_limit; storage_limit })
                   (obj5
                      (req "source" string)
                      (req "fee" string)
                      (req "counter" string)
                      (req "gas_limit" string)
                      (req "storage_limit" string)))

type manager_op_body =
  | Reveal of string
  | Transaction of {
      amount : string ;
      destination : string ;
      entrypoint : string option ;
      parameters : Data_encoding.json option ;
    }
  | Origination of {
      balance : string ;
      delegate : string option ;
      script : Data_encoding.json ;
    }
  | Delegation of string option

let manager_item_encoding_cases =
  Data_encoding.(union
                   [ case ~title:"reveal" Json_only
                       (merge_objs
                          generic_manager_encoding
                          (obj2
                             (req "kind" (constant "reveal"))
                             (req "public_key" string)))
                       (function
                         | (generic, Reveal x) -> Some (generic,((),x))
                         | _ -> None)
                       (fun (generic,((),x)) -> (generic, Reveal x)) ;
                     case ~title:"transaction" Json_only
                       (merge_objs
                          generic_manager_encoding
                          (obj4
                             (req "kind" (constant "transaction"))
                             (req "amount" string)
                             (req "destination" string)
                             (opt "parameters" (obj2
                                                  (req "entrypoint" string)
                                                  (req "value" json)))))
                       (function
                         | (generic,
                            Transaction
                              { amount; destination; entrypoint = None; parameters = None }) ->
                           Some (generic,((),amount,destination,None))
                         | (generic,
                            Transaction
                              { amount; destination; entrypoint = Some e; parameters = None}) ->
                           Some (generic,((),amount,destination,Some (e,`Null)))
                         | (generic,
                            Transaction
                              { amount; destination; entrypoint = None ; parameters = Some v}) ->
                           Some (generic,((),amount,destination,Some ("default",v)))
                         | (generic,
                            Transaction
                              { amount; destination; entrypoint = Some e; parameters = Some v}) ->
                           Some (generic,((),amount,destination,Some (e,v)))
                         | _ -> None)
                       (function
                         | (generic, ((), amount, destination, None)) ->
                           (generic,
                            Transaction
                              { amount ; destination ; entrypoint = None ; parameters = None })
                         | (generic, ((), amount, destination, Some (e,v))) ->
                           (generic,
                            Transaction
                              { amount ; destination ; entrypoint = Some e ; parameters = Some v })) ;
                     case ~title:"origination" Json_only
                       (merge_objs
                          generic_manager_encoding
                          (obj4
                             (req "kind" (constant "origination"))
                             (req "balance" string)
                             (opt "delegate" string)
                             (req "script" json)))
                       (function
                         | (generic, Origination { balance ; delegate ; script }) ->
                           Some (generic,((),balance,delegate,script))
                         | _ -> None)
                       (fun (generic,((),balance,delegate,script)) ->
                          (generic, Origination { balance ; delegate ; script })) ;
                     case ~title:"delegation" Json_only
                       (merge_objs
                          generic_manager_encoding
                          (obj2
                             (req "kind" (constant "delegation"))
                             (opt "delegate" string)))
                       (function
                         | (generic, Delegation x) -> Some (generic,((),x))
                         | _ -> None)
                       (fun (generic,((),x)) -> (generic, Delegation x)) ;
                   ])

type manager_batch = {
  generic : generic_manager ;
  batch : manager_op_body list ;
}

type some_granada_op =
  | Endorsement of { slot : int ; branch : string ; level : int32 }
  | Manager of manager_batch
  | Im_lazy of Data_encoding.json

let granada_op_partial_encoding =
  Data_encoding.(union
                   [case ~title:"endorsement" Json_only
                      (list (obj3
                               (req "kind" (constant "endorsement_with_slot"))
                               (req "endorsement"
                                  (obj3
                                     (req "branch" string)
                                     (req "operations"
                                        (obj2
                                           (req "kind" (constant "endorsement"))
                                           (req "level" int32)))
                                     (req "signature" string)))
                               (req "slot" int16)))
                      (function
                        | Endorsement { branch; level; slot } ->
                          Some [(),(branch,((),level),""),slot]
                        | _ -> None)
                      (function
                        | [(),(branch,((),level),_),slot] ->
                          Endorsement { branch ; level; slot }
                        | _ -> failwith "malformed endorsement");
                    case ~title:"manager" Json_only
                      (list manager_item_encoding_cases)
                      (function
                        | Manager { generic ; batch } ->
                          Some (List.map (fun x -> (generic,x)) batch)
                        | _ -> None)
                      (function
                        | [] -> failwith "malformed manager batch"
                        | (generic,x) :: ops ->
                          let batch = x :: List.map snd ops in
                          Manager { generic ; batch }) ;
                    case ~title:"TODO" Json_only json
                      (function Im_lazy x -> Some x | _ -> None)
                      (fun x -> Im_lazy x)]
                )

let next_op_encoding =
  Data_encoding.(obj4
                  (req "protocol" string)
                  (req "branch" string)
                  (req "contents" granada_op_partial_encoding)
                  (req "signature" string))

(* End of duplication *)

let view_manager_op = function
  | Reveal _ -> Vdom.elt "p" [Vdom.text "reveal"]
  | Transaction _ -> Vdom.elt "p" [Vdom.text "transaction"]
  | Origination _ -> Vdom.elt "p" [Vdom.text "origination"]
  | Delegation _ -> Vdom.elt "p" [Vdom.text "delegation"]

let view_manager _generic batch =
  Vdom.div (List.map view_manager_op batch)

module StringMap = Map.Make(String)

type mempool_slice = {
  endorsements : int list ;
  manager_batches : manager_batch list ;
  stuff : Data_encoding.json list ;
}

let empty_slice = { endorsements = []; manager_batches = []; stuff = [] }

let add_endosement e s = {
  endorsements = e :: s.endorsements ;
  manager_batches = s.manager_batches ;
  stuff = s.stuff ;
}

let add_manager_batch m s = {
  endorsements = s.endorsements ;
  manager_batches = m :: s.manager_batches ;
  stuff = s.stuff ;
}

let add_stuff x s = {
  endorsements = s.endorsements ;
  manager_batches = s.manager_batches ;
  stuff = x :: s.stuff ;
}

type model = {
  mempool_ops : mempool_slice StringMap.t;
  mempool_encoding_size : int;
}

let empty = { mempool_ops = StringMap.empty; mempool_encoding_size = 0 }

let list_of_stream stream =
    let result = ref [] in
    Stream.iter (fun value -> result := value :: !result) stream;
    List.rev !result

let add_op ops branch op =
  let slice =
    Option.value (StringMap.find_opt branch ops)~default:empty_slice in
  let slice' = match op with
    | Endorsement { slot; _ } -> add_endosement slot slice
    | Manager x -> add_manager_batch x slice
    | Im_lazy c -> add_stuff c slice in
  StringMap.add branch slice' ops

let update _ text =
  let size = String.length text in
  let news = list_of_stream (Yojson.Safe.stream_from_string text) in
  let ops =
  List.fold_left
    (fun acc -> function
     | `Bool _
     | `Null
     | `Assoc _
     | `Int _
     | `Intlit _
     | `Tuple _
     | `Variant _
     | `Float _
     | `String _ -> assert false
     | `List news -> List.fold_left (fun a x ->
         let (_,branch,c,_) =
           try
             Data_encoding.(Json.destruct (next_op_encoding) (Json_repr.from_yojson x))
           with x -> ("","",Im_lazy (`String (Format.asprintf "%a@." (Data_encoding.Json.print_error ?print_unknown:None) x)),"") in
       add_op a branch c)
         acc news) StringMap.empty news in
    { mempool_ops = ops; mempool_encoding_size = size }

let view_endorsements endorsements =
  Vdom.elt ~a:[Vdom.class_ "list-group list-group-horizontal"] "ul"
    (List.map
       (fun x -> Vdom.elt ~a:[Vdom.class_ "list-group-item"] "li" [Vdom.text (string_of_int x)])
       endorsements)

let view t =
  Vdom.div ~a:[Vdom.class_ "card"]
    [ Vdom.div ~a:[Vdom.class_ "card-body"]
        (List.concat_map
           (fun (b,{ endorsements ; manager_batches ; stuff }) ->
              ( Vdom.elt ~a:[Vdom.class_ "card-title"] "h5" [Vdom.text b] ::
                view_endorsements endorsements ::
                List.fold_left
                  (fun acc { generic ; batch } -> view_manager generic batch :: acc)
                  (List.rev_map Json_viewer.of_t stuff)
                  manager_batches))
       (StringMap.bindings t.mempool_ops))
    ]
