(*****************************************************************************)
(*                                                                           *)
(* Tezos-node control panel                                                  *)
(*                                                                           *)
(* Copyright (c) 2021 Pierre Boutillier <pierre.boutillier@laposte.net>      *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* Where we duplicate types&encodings because we cannot compile octez to js *)

type network_version = {
  chain_name : string;
  distributed_db_version : int;
  p2p_version : int;
}

let network_version_encoding =
  let open Data_encoding in
  def
    "network_version"
    ~description:
      "A version number for the network protocol (includes distributed DB \
       version and p2p version)"
  @@ conv
       (fun {chain_name; distributed_db_version; p2p_version} ->
         (chain_name, distributed_db_version, p2p_version))
       (fun (chain_name, distributed_db_version, p2p_version) ->
         {chain_name; distributed_db_version; p2p_version})
       (obj3
          (req "chain_name" string)
          (req "distributed_db_version" uint16)
          (req "p2p_version" uint16))

type additional_info = Dev | RC of int | Release

let string_of_additional_info = function
  | Dev -> "+dev"
  | RC n -> "~rc" ^ string_of_int n
  | Release -> ""

type version = {major : int; minor : int; additional_info : additional_info}

let version_to_string {major; minor; additional_info} =
  string_of_int major ^ "." ^ string_of_int minor
  ^ string_of_additional_info additional_info

type commit_info = {commit_hash : string; commit_date : string}

type t = {
  version : version;
  network_version : network_version;
  commit_info : commit_info option;
}

let commit_info_encoding =
  let open Data_encoding in
  conv
    (fun {commit_hash; commit_date} -> (commit_hash, commit_date))
    (fun (commit_hash, commit_date) -> {commit_hash; commit_date})
    (obj2 (req "commit_hash" string) (req "commit_date" string))

(* Locally defined encoding for Version.additional_info *)
let additional_info_encoding =
  let open Data_encoding in
  union
    [
      case
        (Tag 0)
        ~title:"Dev"
        (constant "dev")
        (function Dev -> Some () | _ -> None)
        (fun () -> Dev);
      case
        (Tag 1)
        ~title:"RC"
        (obj1 (req "rc" int31))
        (function RC n -> Some n | _ -> None)
        (fun n -> RC n);
      case
        (Tag 2)
        ~title:"Release"
        (constant "release")
        (function Release -> Some () | _ -> None)
        (fun () -> Release);
    ]

(* Locally defined encoding for Version.t *)
let current_version_encoding =
  let open Data_encoding in
  conv
    (fun {major; minor; additional_info} ->
      (major, minor, additional_info))
    (fun (major, minor, additional_info) -> {major; minor; additional_info})
    (obj3
       (req "major" int31)
       (req "minor" int31)
       (req "additional_info" additional_info_encoding))

let encoding =
  let open Data_encoding in
  conv
    (fun {version; network_version; commit_info} ->
      (version, network_version, commit_info))
    (fun (version, network_version, commit_info) ->
      {version; network_version; commit_info})
    (obj3
       (req "version" current_version_encoding)
       (req "network_version" network_version_encoding)
       (req "commit_info" (option commit_info_encoding)))

(* End of duplication *)

let parse x =
  Data_encoding.Json.destruct
    encoding (Json_repr.from_yojson (Yojson.Safe.from_string x))

let view v =
  Vdom.elt
    "p"
    [Vdom.text
       (version_to_string v.version^" on "^ v.network_version.chain_name)]
