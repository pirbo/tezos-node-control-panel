(*****************************************************************************)
(*                                                                           *)
(* Tezos-node control panel                                                  *)
(*                                                                           *)
(* Copyright (c) 2020-2021 Pierre Boutillier <pierre.boutillier@laposte.net> *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

let json_from_string s = Json_repr.from_yojson (Yojson.Safe.from_string s)

type 'msg Vdom.Cmd.t +=
  | Http_request of {
      meth: string;
      url: string;
      payload: string;
      on_progress: (string -> 'msg) option;
      on_done: (status:int -> string -> 'msg);
    }
  | After of int * 'msg

let http_request
    (type msg) ~meth ~url ~payload ?(on_progress : (string -> msg) option)
    (on_done : status:_ -> _ -> msg) : msg Vdom.Cmd.t =
  Http_request {meth; url; payload; on_progress; on_done}

let after x f = After (x, f)

type panel_selecter =
  | Select_block
  | Select_mempool
  | Select_network
  | Select_worker

type msg =
  | Select_panel of panel_selecter
  | Offline of string
  | Check_connectivity of string option * msg Vdom.Cmd.t list
  | Connected of Version_viewer.t * msg Vdom.Cmd.t list
  | Set_head of Data_encoding.json
  | Mempool_chunk of string
  | Invalid_head of string
  | Head_changed

type panel =
  | Block_explorer of Data_encoding.json
  | Mempool_explorer of Mempool_viewer.model
  | Network_explorer
  | Worker_explorer

type model = {
  version : Version_viewer.t option;
  errors : string list;
  active_panel : panel;
}

let button txt msg =
  Vdom.input
    [] ~a:[Vdom.onclick (fun _ -> msg); Vdom.type_button; Vdom.value txt]

let get_head =
  http_request ~meth:"GET" ~url:"/chains/main/blocks/head" ~payload:""
    (fun ~status x ->
       if status <> 200 then Invalid_head x
       else Set_head (json_from_string x))

let monitor_mempool ?on_progress () =
  http_request ~meth:"GET" ~url:"/chains/main/mempool/monitor_operations?applied=true&branch_refused=false&branch_delayed=false&refused=false" ~payload:""
    ?on_progress
    (fun ~status text -> if status <> 200
      then Check_connectivity (Some text,[After (10,Head_changed)])
      else Head_changed)

let init =
  Vdom.return { version = None; errors = []; active_panel = Mempool_explorer Mempool_viewer.empty }

let view model = Vdom.div [
    Vdom.elt "nav" ~a:[Vdom.class_ "navbar navbar-light"] (
      Vdom.div ~a:[Vdom.class_ "navbar-nav"] [
        Vdom.elt
          ~a:((match model.active_panel with
              | Mempool_explorer _ -> Vdom.add_class "active"
              | _ -> fun x -> x)
               [Vdom.class_ "nav-item"; Vdom.onclick
                  (fun _ -> Select_panel Select_mempool)])
          "a" [Vdom.text "mempool"];
        Vdom.elt
          ~a:((match model.active_panel with
              | Block_explorer _ -> Vdom.add_class "active"
              | _ -> fun x -> x)
               [Vdom.class_ "nav-item"; Vdom.onclick
                  (fun _ -> Select_panel Select_block)])
          "a" [Vdom.text "blocks"];
        Vdom.elt
          ~a:((match model.active_panel with
              | Network_explorer -> Vdom.add_class "active"
              | _ -> fun x -> x)
               [Vdom.class_ "nav-item"; Vdom.onclick
                  (fun _ -> Select_panel Select_network)])
          "a" [Vdom.text "network"];
        Vdom.elt
          ~a:((match model.active_panel with
              | Worker_explorer -> Vdom.add_class "active"
              | _ -> fun x -> x)
               [Vdom.class_ "nav-item";
                Vdom.onclick (fun _ -> Select_panel Select_worker)])
          "a" [Vdom.text "workers"];
      ]::
    (match model.version with
     | None -> [
         Vdom.elt "h1" [ Vdom.text "Not connected" ] ;
         let on_progress = match model.active_panel with
           | Mempool_explorer _ -> Some (fun x -> Mempool_chunk x)
           | _ -> None in
         button "retry"
           (Check_connectivity (None, [monitor_mempool ?on_progress ()]));
       ]
     | Some version -> [
         Version_viewer.view version;
         button "Stop" (Offline "");
       ]));
    Vdom.div (List.map (fun err -> Vdom.elt "p" [Vdom.text err]) model.errors);
    Vdom.div ~a:[Vdom.class_ "container-fluid"]
      (match model.active_panel with
       | Block_explorer head -> [ Json_viewer.of_t head ]
       | Mempool_explorer mempool -> [ Mempool_viewer.view mempool ]
       | Network_explorer -> []
       | Worker_explorer -> []);
  ]

let update model = function
  | Offline err ->
    Vdom.return { version = None; errors = [err]; active_panel = model.active_panel }
  | Select_panel x ->
    let active_panel,c = match x with
      | Select_block ->
        Block_explorer `Null, [get_head; monitor_mempool ?on_progress:None ()]
      | Select_mempool ->
        Mempool_explorer Mempool_viewer.empty,
        [monitor_mempool ~on_progress:(fun x -> Mempool_chunk x) ()]
      | Select_network -> Network_explorer, []
      | Select_worker -> Worker_explorer, [] in
    Vdom.return
      ~c
      { model with active_panel}
  | Check_connectivity (err,k) ->
    Vdom.return
      ~c:[http_request ~meth:"GET" ~url:"/version" ~payload:""
            (fun ~status x ->
               if status <> 200 then Offline x
               else
                 let version = Version_viewer.parse x in
                 Connected (version,k))]
      { model with errors = Option.to_list err @ model.errors}
  | Connected (version,c) ->
    Vdom.return
      ~c
      { version = Some version ; errors = model.errors; active_panel = model.active_panel }
  | Set_head head ->
    (match model.active_panel with
     | Block_explorer _ ->
       Vdom.return
         { version = model.version ; errors = model.errors; active_panel = Block_explorer head }
     | _ -> Vdom.return model)
  | Invalid_head err ->
    (match model.active_panel with
     | Block_explorer _ ->
       Vdom.return
         { version = model.version ; errors = model.errors; active_panel = Block_explorer (`String err) }
     | _ -> Vdom.return model)
  | Head_changed ->
    (match model.active_panel with
     | Block_explorer _ ->
       let c = match model.version with
         | None -> []
         | Some _ -> [get_head; monitor_mempool ?on_progress:None ()] in
       Vdom.return ~c model
     | Mempool_explorer _ ->
       let model =
         { version = model.version ; errors = model.errors; active_panel = Mempool_explorer Mempool_viewer.empty } in
       let c = match model.version with
         | None -> []
         | Some _ -> [monitor_mempool ~on_progress:(fun x -> Mempool_chunk x) ()] in
       Vdom.return ~c model
     | _ -> Vdom.return model)
  | Mempool_chunk text ->
    (match model.active_panel with
     | Mempool_explorer mempool ->
       Vdom.return
         { version = model.version ; errors = model.errors; active_panel = Mempool_explorer (Mempool_viewer.update mempool text) }
     | _ -> Vdom.return model)

let app = Vdom.app ~init ~update ~view ()

let run_http_request ~meth ~url ~payload ?on_progress ~on_done () =
  let open Js_browser.XHR in
  let r = create () in
  open_ r meth url;
  set_response_type r "text";
  (match on_progress with
   | None -> ()
   | Some f -> set_onprogress r (fun () -> f (response_text r)));
  set_onreadystatechange r
    (fun () ->
       match ready_state r with
       | Done -> on_done ~status:(status r) (response_text r)
       | _ ->
         ()
    );
  send r (Ojs.string_to_js payload)

let cmd_handler ctx = function
  | Http_request {meth; url; payload; on_progress; on_done} ->
    let on_progress =
      Option.map (fun f -> fun s -> Vdom_blit.Cmd.send_msg ctx (f s)) on_progress in
    run_http_request
      ~meth ~url ~payload ?on_progress
      ~on_done:(fun ~status s ->
          Vdom_blit.Cmd.send_msg ctx (on_done ~status s)) ();
    true
  | After (n, msg) ->
    ignore
      Js_browser.(Window.set_timeout
                    window (fun () -> Vdom_blit.Cmd.send_msg ctx msg) n);
    true
  | _ -> false

let () = Vdom_blit.(register (cmd { Cmd.f = cmd_handler }))

let run () =
  let blit_app = Vdom_blit.run app in
  let dom_elt = Vdom_blit.dom blit_app in
  let () =
    Js_browser.Element.append_child
      Js_browser.(Document.body document) dom_elt in
  let () = Vdom_blit.process
      blit_app
      (Check_connectivity (None,[monitor_mempool ~on_progress:(fun x -> Mempool_chunk x) ()])) in
  ()

let () =
  Js_browser.Window.set_onload Js_browser.window run
