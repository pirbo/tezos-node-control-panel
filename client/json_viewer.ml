(*****************************************************************************)
(*                                                                           *)
(* Tezos-node control panel                                                  *)
(*                                                                           *)
(* Copyright (c) 2020 Pierre Boutillier <pierre.boutillier@laposte.net>      *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

let rec add_unique x = function
  | [] -> [x]
  | h :: t as l ->
    if String.equal x h
    then l
    else let o = add_unique x t in if o == t then l else h::o

let grab_headers x = function
  | `Null | `String _ | `Bool _ | `Float _ | `A _ -> None
  | `O l -> Option.map
              (fun (heads,bodies) ->
                 (List.fold_left (fun acc (n,_) -> add_unique n acc) heads l,
                 l::bodies)) x

let rec of_t = function
  | `Null -> Vdom.text "null"
  | `String s -> Vdom.text s
  | `Bool b -> if b then Vdom.text "true" else Vdom.text "false"
  | `Float f -> Vdom.text (string_of_float f)
  | `O l ->
    Vdom.elt "dl"
      (List.concat_map
         (fun (n,x) -> [ Vdom.elt "dt" [Vdom.text n]; Vdom.elt "dd" [of_t x]])
         l)
  | `A l ->
    match List.fold_left grab_headers (Some ([],[])) l with
    | None -> Vdom.elt "ul" (List.map (fun x -> Vdom.elt "li" [of_t x]) l)
    | Some (heads,bodies) ->
      Vdom.elt "table" (
        Vdom.elt "tr" (List.map (fun x -> Vdom.elt "th" [Vdom.text x]) heads)
        :: List.map
          (fun r ->
             Vdom.elt "tr"
               (List.map
                  (fun head ->
                     Vdom.elt "td"
                       (Option.to_list
                          (Option.map of_t (List.assoc_opt head r))))
                  heads))
          bodies)
