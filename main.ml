(*****************************************************************************)
(*                                                                           *)
(* Tezos-node control panel                                                  *)
(*                                                                           *)
(* Copyright (c) 2020 Pierre Boutillier <pierre.boutillier@laposte.net>      *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Lwt.Infix

let proxy_callback endpoint _conn req body =
  (* Stolen from cohttp_proxy_lwt (cohttp-lwt-unix example) *)
  let uri =
    let req_uri = Cohttp.Request.uri req in
    let path = if Uri.path endpoint = ""
      then Some (Uri.path req_uri)
      else Some (Uri.path endpoint ^ "/" ^ Uri.path req_uri) in
    Uri.with_uri
      ~path ~query:(Some (Uri.query req_uri)) ~fragment:(Uri.fragment req_uri)
      endpoint in
  (* Strip out hop-by-hop connection headers *)
  let headers =
    Cohttp.Request.headers req |> fun h ->
    (*Cohttp.Header.remove h "accept-encoding" |> fun h ->*)
    Cohttp.Header.remove h "content-length" |> fun h ->
    Cohttp.Header.remove h "transfer-encoding" |> fun h ->
    Cohttp.Header.remove h "connection" (*|> fun h ->
    (Cohttp.Header.add h "accept-encoding" "identity"*)
  in
  (* Fetch the remote URI *)
  let meth = Cohttp.Request.meth req in
  Cohttp_lwt_unix.Client.call ?ctx:None ~headers ~body ?chunked:None meth uri >>=
  fun (resp,body) ->
  let status = Cohttp.Response.status resp in
  let headers =
    Cohttp.Response.headers resp |> fun h ->
    Cohttp.Header.remove h "transfer-encoding" |> fun h ->
    Cohttp.Header.remove h "content-length" |> fun h ->
    Cohttp.Header.remove h "connection"
  in
  Cohttp_lwt_unix.Server.respond ~headers ?flush:None ~status ~body ()

let callback endpoint conn req body =
  let req_uri = Cohttp.Request.uri req in
  match Uri.path req_uri with
  | "/" | "/index.html" ->
    let headers = Cohttp.Header.init_with "content-type" "text/html" in
    Cohttp_lwt_unix.Server.respond_string ?flush:None ~headers ~status:`OK ~body:Files.index_html ()
  | "/client.bc.js" ->
    let headers = Cohttp.Header.init_with "content-type" "application/javascript" in
    Cohttp_lwt_unix.Server.respond_string ?flush:None ~headers ~status:`OK ~body:Files.client_bc_js ()
  | _ ->
    proxy_callback endpoint conn req body

let start_server endpoint port =
  let (stop,stopper) = Lwt.task () in
  let _ = Sys.signal Sys.sigint (Sys.Signal_handle (fun _ -> Lwt.wakeup stopper ())) in
  let callback = callback (Uri.of_string endpoint) in
  let server =
    Cohttp_lwt_unix.Server.create
      ?timeout:None ?backlog:None ~stop ?on_exn:None ?ctx:None ~mode:(`TCP (`Port port))
      (Cohttp_lwt_unix.Server.make ?conn_closed:None ~callback ()) in
  Lwt_main.run server

let cmd =
  let open Cmdliner in
  let endpoint =
    let doc = "RPC server address" in
    Arg.(value & opt string "http://localhost:8732" & info ["endpoint"] ~docv:"URI" ~doc) in
  let port =
    let doc = "TCP port the server listen to" in
    Arg.(value & opt int 18732 & info ["p"] ~docv:"PORT" ~doc) in
  (Term.(pure start_server $ endpoint $ port),
   Term.info "tezos-node-control-panel" ~doc:"A WebGUI for tezos-node")


let () = match Cmdliner.(Term.eval cmd) with
  | `Help | `Ok () | `Version -> ()
  | `Error _ -> exit 1
